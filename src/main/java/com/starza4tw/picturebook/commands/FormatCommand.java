package com.starza4tw.picturebook.commands;

import com.starza4tw.picturebook.ConfigurationManager;
import com.starza4tw.picturebook.filter.FilterUtilities;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class FormatCommand  implements CommandExecutor
{
    @Override public CommandResult execute(CommandSource source, CommandContext args) throws CommandException
    {
        if(source instanceof Player)
        {
            final Player player = ((Player) source);
            final ItemStack stack = player.getItemInHand(HandTypes.MAIN_HAND).get();

            // The following code is to meant to filter book text, but requires https://github.com/SpongePowered/SpongeCommon/issues/509 to be fixed first.

            //            if(args.<String>getOne("formatMode").get() == "book")
            //            {
            //                if(player.hasPermission("picturebook.module.book") && ConfigurationManager.useBookModule)
            //                {
            //                    if(stack.getItem() == ItemTypes.WRITABLE_BOOK)
            //                    {
            //                        //List<Text> pages = stack.get(Keys.BOOK_PAGES).get();
            //                        List<Text> test = new ArrayList<Text>();
            //                        test.add(Text.of("Test"));
            //                        //for(int pageNumber = 0; pageNumber < pages.size(); pageNumber++)
            //                        //{
            //                        //    pages.set(pageNumber, FilterUtilities.color(FilterUtilities.filter(pages.get(pageNumber).toPlain())));
            //                        //    Main.getLogger().debug(FilterUtilities.filter(pages.get(pageNumber).toPlain()));
            //                        //}
            //                        stack.offer(Keys.BOOK_PAGES, test);
            //                        player.setItemInHand(HandTypes.MAIN_HAND, stack);
            //                    }
            //                }
            //            }
            if(args.<String>getOne("formatMode").get() == "item")
            {
                if(player.hasPermission("picturebook.module.item") &&  ConfigurationManager.useItemModule)
                {
                    stack.offer(Keys.DISPLAY_NAME, FilterUtilities.format(FilterUtilities.filter(stack.get(Keys.DISPLAY_NAME).get().toPlain())));
                    player.setItemInHand(HandTypes.MAIN_HAND, stack);
                    return CommandResult.success();
                }
                else
                {
                    throw new CommandException(Text.of(TextColors.RED, "You are not allowed to use that command."));
                }
            }
            else
            {
                return CommandResult.empty(); //This should never happen.
            }
        }
        else
        {
            throw new CommandException(Text.of(TextColors.RED, "This command can only be used by players!"));
        }
    }
}