package com.starza4tw.picturebook;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;

public class ReloadListener 
{
    @Listener public void onReload(GameReloadEvent event)
    {
        ConfigurationManager.FilterMap.clear();
        ConfigurationManager.loadConfiguration();
        Main.getInstance().logger.info("[PB] Picturebook was successfully reloaded.");
    }
}