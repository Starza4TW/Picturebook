package com.starza4tw.picturebook.filter;

import com.starza4tw.picturebook.ConfigurationManager;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.filter.cause.First;

public class CommandListener
{
    @Listener public void onCommand(SendCommandEvent event, @First Player player)
    {
        if(player.hasPermission("picturebook.module.command") && ConfigurationManager.useCommandModule)
        {
            if(!event.getArguments().equals(""))
            {
                event.setArguments(FilterUtilities.filter(event.getArguments()));
            }
        }
    }
}