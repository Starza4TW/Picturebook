package com.starza4tw.picturebook.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;

import com.starza4tw.picturebook.ConfigurationManager;

public class AddCommand implements CommandExecutor
{
	@Override public CommandResult execute(CommandSource source, CommandContext args) throws CommandException 
	{
		ConfigurationManager.FilterMap.put(args.<String>getOne("key").get(), args.<String>getOne("value").get());
		ConfigurationManager.saveConfiguration();
		source.sendMessage(Text.of("Added entry to filter."));
		return CommandResult.success();
	}
}