package com.starza4tw.picturebook.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import com.starza4tw.picturebook.ConfigurationManager;

public class RemoveCommand implements CommandExecutor
{
	@Override public CommandResult execute(CommandSource source, CommandContext args) throws CommandException 
	{
	    if(ConfigurationManager.FilterMap.containsKey(args.<String>getOne("key").get()))
	    {
	        ConfigurationManager.FilterMap.remove(args.<String>getOne("key").get());
	        ConfigurationManager.deleteNode(new Object[] {"filter", args.<String>getOne("key").get()});
	        ConfigurationManager.saveConfiguration();
	        source.sendMessage(Text.of("Removed entry from filter."));
	        return CommandResult.success();
	    }
	    else // The filter does not contain the key
	    {
	        throw new CommandException(Text.of(TextColors.RED, "The supplied key is not part of the filter, and therefore can't be removed from it."));
	    }
	}
}