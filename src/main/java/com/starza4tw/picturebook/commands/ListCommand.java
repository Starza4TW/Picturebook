package com.starza4tw.picturebook.commands;

import com.starza4tw.picturebook.ConfigurationManager;
import com.starza4tw.picturebook.Main;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListCommand implements CommandExecutor 
{	
	@Override public CommandResult execute(CommandSource source, CommandContext args) throws CommandException 
	{
	    List<Text> filterList = new ArrayList<Text>();
		for(String key : ConfigurationManager.FilterMap.keySet())
		{
		    filterList.add(Text.of(key + " = " + ConfigurationManager.FilterMap.get(key)));
		}
		Collections.sort(filterList, (textOne, textTwo) -> textOne.toPlain().compareToIgnoreCase(textTwo.toPlain())); //Sorts the list alphabetically.
	    Main.getInstance().paginationService.builder().title(Text.of(TextColors.GOLD, "Picturebook Variables")).contents(filterList).padding(Text.of(TextColors.BLUE, "=")).sendTo(source);
		return CommandResult.success();
		
	}
}