package com.starza4tw.picturebook;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;

import javax.annotation.Nullable;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;

public class ConfigurationManager 
{
    public static HashMap<String, String> FilterMap = new HashMap<String, String>();

    public static ConfigurationLoader<CommentedConfigurationNode> configurationLoader = null;

    public static CommentedConfigurationNode rootNode = null;

    public static Path configurationPath;

    public static Boolean useCommandModule;

    public static Boolean useChatModule;

    public static Boolean useItemModule;

    public static Boolean useSignModule;

    public static void initializeConfiguration(Path configurationDirectory)
    {     
        if (!configurationDirectory.toFile().exists())
        {
            try
            {
                Main.getInstance().pluginContainer.getAsset("picturebook.conf").get().copyToFile(configurationDirectory);
            } 
            catch (IOException exception) 
            {
                exception.printStackTrace();
            }
        }
        
        configurationLoader = HoconConfigurationLoader.builder().setPath(configurationDirectory).build();
        
        try 
        {
            rootNode = configurationLoader.load();
        }
        catch (IOException exception) 
        {
            exception.printStackTrace();
        }
        
        //This code allows me to get around numbering configuration files. Any new modules will just added to the existing file.
        addNode(new Object[] {"modules", "chat"}, true);
        addNode(new Object[] {"modules", "command"}, true);
        addNode(new Object[] {"modules", "item"}, true);
        addNode(new Object[] {"modules", "sign"}, true);
        saveConfiguration();
    }

    public static void loadConfiguration()
    {
        for(Object key : rootNode.getNode("filter").getChildrenMap().keySet())
        {
            FilterMap.put((String)key, rootNode.getNode("filter", key).getString());
        }
        useChatModule = rootNode.getNode("modules", "chat").getBoolean();
        useCommandModule = rootNode.getNode("modules", "command").getBoolean();
        useItemModule = rootNode.getNode("modules", "item").getBoolean();
        useSignModule = rootNode.getNode("modules", "sign").getBoolean();
    }

    public static void saveConfiguration()
    {        
        for(String key : FilterMap.keySet())
        {
            addNode(new Object[] {"filter", key}, FilterMap.get(key));
        }

        try 
        {
            configurationLoader.save(rootNode);
        }
        catch (IOException exception) 
        {
            exception.printStackTrace();
        }
    }

    public static void addNode(Object[] path, @Nullable Object value) //Used only for adding nodes. The node must not exist for this method to run.
    {
        if (rootNode.getNode(path).isVirtual() == true) 
        {
            rootNode.getNode(path).setValue(value);
        }
    }

    public static void editNode(Object[] path, Object value) //Used only for editing existing nodes.
    {
        if (rootNode.getNode(path).isVirtual() == false) 
        {
            rootNode.getNode(path).setValue(value);
        }
    }

    public static void deleteNode(Object[] path) //Used only for deleting existing nodes.
    {
        if (rootNode.getNode(path).isVirtual() == false) 
        {
            rootNode.getNode(path).setValue(null);
        }
    }
}