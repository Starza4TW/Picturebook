package com.starza4tw.picturebook.filter;

import com.starza4tw.picturebook.ConfigurationManager;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.chat.ChatType;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class FilteredMessageChannel implements MessageChannel
{	
    private Set<MessageReceiver> members = new HashSet<MessageReceiver>();
    
    @Override public Optional<Text> transformMessage(Object sender, MessageReceiver recipient, Text text, ChatType chatType) 
    {
        if(recipient instanceof Player)
        {
            if(((Player) recipient).hasPermission("picturebook.module.chat") && ConfigurationManager.useChatModule)
            {
                return Optional.of(FilterUtilities.format(FilterUtilities.filter(FilterUtilities.simplify(text))));
            }
            else
            {
                return Optional.of(text);
            }
        }
        return Optional.empty();
    }

    @Override public Collection<MessageReceiver> getMembers() 
    {
        return members;
    }
}