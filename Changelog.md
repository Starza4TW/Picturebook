Changelog for Picturebook:

Version 2.1.0 - 5 September 2016
[Information]
- Configuration files will be regenerated in this release. Please move your data from "Picturebook.conf" to "picturebook.conf"
[Additions]
- The simplify() method in FilterUtilities.
- PluginContainer for use in the new ConfigurationManager.
[Changes]
- Logger is now injected.
- Classes other than Main now must acess plugin data using getInstance().
- SignListener & CommandListener now only fire if a player caused the event. Fixes #5.
- ConfigurationManager now copies the configuration from a default file and adds modules according to the plugin version. Fixes #4.
- RemoveCommand now checks to see if the supplied key is found in the filter before removing it.
- The onChat() event previously held in ChatListener has been moved to Main.
- The method known as color() found in FilterUtilities is now known as format().
- A few other minor things.
[Removals]
- The awful ChatListener class and its headers/bodies/footers.

Version 2.0.0 - 22 June 2016
[Information]
- Intial Release of Picturebook for the Sponge API.
[Additions]
- Picturebook command (/picturebook or /pb)
- Subcommands add, format, list, module, and remove.
- Support for standard Minecraft formatting codes.
- Variables for use within chat, commands, sign text, and item names.
- Configurable modules to turn on/off modules of the plugin.
- Several permissions for a variety of features within Picturebook.